GHT API Client Bundle
=========================

This bundle provides the [GHT API Client](https://bitbucket.org/greenhollowtech/ght-api-client) as a service in Symfony.

# Installation

## Get the Composer package

To install with [Composer](https://getcomposer.org/), run `composer require greenhollowtech/ght-api-client-bundle`.

## Add the GHTApiClientBundle to your Symfony application

```php
// app/AppKernel.php

    public function registerBundles()
    {
        return array(
            // ...
            new GHT\ApiClientBundle\GHTApiClientBundle(),
            // ...
        );
    }
```

# Configuration

## Host configuration

You can configure one or more hosts.  Each must include at least a domain:

```yml
# app/config/config.yml

ght_api_client:
    hosts:
        my_api_host:
            domain: my.apidomain.com
```

If making HTTPS requests, set the port to 443:

```yml
            domain: my.apidomain.com
            port: 443
```

If using any other port, you can also set the client to make HTTPS requests by setting the `secure` flag.  Note that setting this flag without setting the port will be ignored, since the port defaults to 80.

```yml
            domain: my.apidomain.com
            port: 9443
            secure: true
```

If making HTTPS requests to a sandbox server, where the SSL is self-signed, you can trigger the client to suppress SSL verification:

```yml
            domain: my.apidomain.com
            port: 443
            sandbox: true
```

## User Entity Configuration

Add the `apiKey` and `apiSecret` properties to your user entity, along with the related getters and setters.

If you already have properties established, and wish to use the existing data, simply configure the API Authenticator to use those properties instead of the default:

```yml
# app/config/config.yml

ght_api_client:
    user_entity:
        api_key_property: myKeyPropertyName
        api_secret_property: mySecretPropertyName
```

The standard `username` property is also expected.  If yours is different, you can set that as well.

```yml
# app/config/config.yml

ght_api_client:
    user_entity:
        api_name_property: myUserName
```

## Default cURL Options Configuration

Add any cURL options that should be set for every request.  There are a few options that are set internally and thus ignored by the client (listed in [the client documentation](https://bitbucket.org/greenhollowtech/ght-api-client)).

```yml
# app/config/config.yml

ght_api_client:
    curl_options:
        connecttimeout: 20
        timeout: 30
```

# Usage

The simplest use of the API client service is to get or post with the host and user included in the parameters:

```php
$apiClientService = $this->get('ght_api_client');

$data = array('some' => 'data to send');

$result = $apiClientService->get('api/path', $data, 'my_api_host', $this->getUser());
```

This is the same as using the `setHost` and `setUser` methods:

```php
$result = $apiClientService->setHost('my_api_host')
    ->setUser($this->getUser())
    ->get('api/path', $data)
;
```

Once set, the host and user will continue to be used for successive requests:

```php
$response = $apiClientService->get('api/status', array(), 'my_api_host', $this->getUser());

// JSON response is decoded into an array
if (!empty($response['status']) && $response['status'] === 'ready') {
    $response = $apiClientService->post('api/some/action', array('some' => 'data to post'));
}
```

You can also configure a new host on the fly, in case of dynamic target hosts, which will then be available by referencing the domain name as you would any configured host name in any `get` or `post` method call:

```php
$apiClientService->setHost('42.10.20.30', 443);

// Check some other API
$response = $apiClientService->get('api/status', array(), 'my_api_host', $this->getUser());

if (empty($response['status']) || $response['status'] !== 'ready') {
    // Make a call to the dynamically set host, same user / API credentials, in this case
    $response = $apiClientService->post('dynamic/api/alert', array(), '42.10.20.30');
}
```

The cURL options can be overridden:

```php
$apiClientService->setCurlOptions(array('connecttimeout' => 10));
...
// Reset to the default configurations for subsequent requests
$apiClientService->setCurlOptions();
```

The GHTApiClient the service uses can also be extended and set.  See the method docs in the class for more.
