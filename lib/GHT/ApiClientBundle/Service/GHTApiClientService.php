<?php

namespace GHT\ApiClientBundle\Service;

use GHT\ApiClient\Entity\ApiConnector;
use GHT\ApiClient\Entity\CurlConfig;
use GHT\ApiClient\GHTApiClient;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException as DependencyInjectionException;
use Symfony\Component\Validator\Constraints\Url as UrlConstraint;
use Symfony\Component\Validator\Validation;

/**
 * Handles authenticated API requests.
 */
class GHTApiClientService
{
    /**
     * @var string
     */
    protected $apiDomain;

    /**
     * @var string
     */
    protected $apiPort;

    /**
     * @var boolean
     */
    protected $apiSandbox;

    /**
     * @var boolean
     */
    protected $apiSecure;

    /**
     * @var string
     */
    protected $apiUserKey;

    /**
     * @var string
     */
    protected $apiUserName;

    /**
     * @var string
     */
    protected $apiUserSecret;

    /**
     * @var \GHT\ApiClient\GHTApiClient
     */
    protected $client;

    /**
     * @var \GHT\ApiClient\Entity\CurlConfig
     */
    protected $curlConfig;

    /**
     * @var array
     */
    protected $hosts;

    /**
     * @var string
     */
    protected $userKeyProperty;

    /**
     * @var string
     */
    protected $userNameProperty;

    /**
     * @var string
     */
    protected $userSecretProperty;

    /**
     * The constructor.
     *
     * @param array $hosts The configured hosts.
     */
    public function __construct($hosts, $userNameProperty, $userKeyProperty, $userSecretProperty)
    {
        $this->hosts = $hosts;
        $this->userKeyProperty = $userKeyProperty;
        $this->userNameProperty = $userNameProperty;
        $this->userSecretProperty = $userSecretProperty;
    }

    /**
     * Make a DELETE request.
     *
     * @param string $path The API path.
     * @param array $data The API data.
     * @param string $host The configured host name, if not already set.
     * @param object $user The user entity, if not already set.
     *
     * @return array
     */
    public function delete($path, $data = array(), $host = null, $user = null)
    {
        $this->init($host, $user);

        return json_decode($this->client->delete($path, $data), true);
    }

    /**
     * Make a GET request.
     *
     * @param string $path The API path.
     * @param array $data The API data.
     * @param string $host The configured host name, if not already set.
     * @param object $user The user entity, if not already set.
     *
     * @return array
     */
    public function get($path, $data = array(), $host = null, $user = null)
    {
        $this->init($host, $user);

        return json_decode($this->client->get($path, $data), true);
    }

    /**
     * Get an instance of the API Client.
     *
     * @return \GHT\ApiClient\GHTApiClient
     */
    public function getClient()
    {
        if ($this->client instanceof GHTApiClient) {
            if ($this->curlConfig instanceof CurlConfig) {
                $this->client->setCurlConfig($this->curlConfig);
            }

            return $this->client;
        }

        if (empty($this->apiDomain)) {
            throw new \UnexpectedValueException('API Client requires a host.');
        }

        if (empty($this->apiUserKey) || empty($this->apiUserName) || empty($this->apiUserSecret)) {
            throw new \UnexpectedValueException('API Client requires a user with credentials.');
        }

        $url = sprintf(
            'http%s://%s%s',
            ($this->apiSecure ? 's' : ''),
            $this->apiDomain,
            ($this->apiPort ? ':' . $this->apiPort : '')
        );

        $connector = new ApiConnector($url, $this->apiUserName, $this->apiUserKey, $this->apiUserSecret);
        $this->client = new GHTApiClient($connector, $this->apiSandbox);
        if ($this->curlConfig instanceof CurlConfig) {
            $this->client->setCurlConfig($this->curlConfig);
        }

        return $this->client;
    }

    /**
     * Make a GET request streamed to a file resource.
     *
     * @param string $path The API path.
     * @param array $data The API data.
     * @param string $host The configured host name, if not already set.
     * @param object $user The user entity, if not already set.
     *
     * @return resource
     */
    public function getToFile($path, $data = array(), $host = null, $user = null)
    {
        $this->init($host, $user);

        return $this->client->getToFile($path, $data);
    }

    /**
     * Get the getter method for a given property of the user entity.
     *
     * @param object $user The user entity.
     * @param string $property The property to be gotten.
     *
     * @return string
     */
    protected function getUserGetter($user, $property)
    {
        $getter = 'get' . strtoupper(substr($property, 0, 1)) . substr($property, 1);

        if (!method_exists($user, $getter)) {
            throw new DependencyInjectionException(sprintf(
                'Configured property "%s" not set in %s',
                $property,
                get_class($user)
            ));
        }

        return $getter;
    }

    /**
     * Perform some common tasks prior to making a request.
     *
     * @param string $host The configured host name.
     * @param object $user The user entity.
     */
    protected function init($host = null, $user = null)
    {
        if ($host) {
            $this->setHost($host);
        }

        if ($user) {
            $this->setUser($user);
        }

        // Trigger the instantiation of a new client if needed
        $this->getClient();
    }

    /**
     * Make a POST request.
     *
     * @param string $path The API path.
     * @param array $data The API data.
     * @param string $host The configured host name, if not already set.
     * @param object $user The user entity, if not already set.
     *
     * @return array
     */
    public function post($path, $data = array(), $host = null, $user = null)
    {
        $this->init($host, $user);

        return json_decode($this->client->post($path, $data), true);
    }

    /**
     * Make a POST request streamed to a file resource.
     *
     * @param string $path The API path.
     * @param array $data The API data.
     * @param string $host The configured host name, if not already set.
     * @param object $user The user entity, if not already set.
     *
     * @return resource
     */
    public function postToFile($path, $data = array(), $host = null, $user = null)
    {
        $this->init($host, $user);

        return $this->client->postToFile($path, $data);
    }

    /**
     * Make a PUT request.
     *
     * @param string $path The API path.
     * @param array $data The API data.
     * @param string $host The configured host name, if not already set.
     * @param object $user The user entity, if not already set.
     *
     * @return array
     */
    public function put($path, $data = array(), $host = null, $user = null)
    {
        $this->init($host, $user);

        return json_decode($this->client->put($path, $data), true);
    }

    /**
     * Set the ApiClient to be used manually.  Allows the ApiClient to be
     * extended without needing to also extend this service.  Also useful for
     * unit testing.
     *
     * Note that setting the host or user will null the set client.
     *
     * @param \GHT\ApiClient\GHTApiClient $client The ApiClient to use for requests.
     *
     * @return \GHT\ApiClientBundle\Service\GHTApiClientService
     */
    public function setClient(GHTApiClient $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Set the cURL options to be used on all subsequent requests.  The first
     * time this is called sets the default options.  Subsequent calls will
     * update override options only, leaving the default options intact.
     *
     * @param array $options The cURL options.
     *
     * @return \GHT\ApiClientBundle\Service\GHTApiClientService
     */
    public function setCurlOptions(array $options = array())
    {
        if (!($this->curlConfig instanceof CurlConfig)) {
            $this->curlConfig = new CurlConfig($options);
        }
        else {
            $this->curlConfig->setOverrideOptions($options);
        }

        return $this;
    }

    /**
     * Set the target host domain and port.
     *
     * @param string $hostName The configured host name or new domain name.
     * @param integer $port The port (ignored if $hostName is a configured host).
     * @param boolean $secure Secure mode - see this bundle's documentation
     *        (ignored if $hostName is a configured host).
     * @param boolean $sandbox Sandbox mode - see this bundle's documentation
     *        (ignored if $hostName is a configured host).
     *
     * @return \GHT\ApiClientBundle\Service\GHTApiClientService
     */
    public function setHost($hostName, $port = null, $secure = false, $sandbox = false)
    {
        if (!isset($this->hosts[$hostName])) {
            // Unconfigured host - check if it's a valid URL
            $protocol = ($port && ($port === 443 || ($port !== 80 && $secure))) ? 'https' : 'http';
            $hostName = trim($hostName, '/');
            $url = sprintf('%s://%s', $protocol, $hostName);

            $urlConstraint = new UrlConstraint();
            $validator = Validation::createValidator();
            $violations = $validator->validate($url, $urlConstraint);

            if (count($violations)) {
                throw new \InvalidArgumentException(sprintf(
                    'Host "%s" is not configured, attempted to validate as a domain: "%s"  %s',
                    $hostName,
                    $url,
                    $violations[0]->getMessage()
                ));
            }

            // Add the host to the configured hosts
            $this->hosts[$hostName] = array(
                'domain' => $hostName,
                'port' => $port ? $port : 80,
                'secure' => $secure,
                'sandbox' => $sandbox,
            );
        }

        $this->apiDomain = $this->hosts[$hostName]['domain'];

        if ($this->hosts[$hostName]['port'] === 80) {
            $this->apiPort = null;
            $this->apiSecure = false;
        }
        elseif ($this->hosts[$hostName]['port'] === 443) {
            $this->apiPort = null;
            $this->apiSecure = true;
        }
        else {
            $this->apiPort = $this->hosts[$hostName]['port'];
            $this->apiSecure = isset($this->hosts[$hostName]['secure']) ? $this->hosts[$hostName]['secure'] : false;
        }

        $this->apiSandbox = isset($this->hosts[$hostName]['sandbox']) ? $this->hosts[$hostName]['sandbox'] : false;

        // Nullify any current client so a fresh one will be instantiated
        $this->client = null;

        return $this;
    }

    /**
     * Set the user making the API request.
     *
     * @param object $user The user entity.
     *
     * @return \GHT\ApiClientBundle\Service\GHTApiClientService
     */
    public function setUser($user)
    {
        // Get the user's API credentials
        $getter = $this->getUserGetter($user, $this->userNameProperty);
        $this->apiUserName = $user->$getter();

        $getter = $this->getUserGetter($user, $this->userKeyProperty);
        $this->apiUserKey = $user->$getter();

        $getter = $this->getUserGetter($user, $this->userSecretProperty);
        $this->apiUserSecret = $user->$getter();

        // Nullify any current client so a fresh one will be instantiated
        $this->client = null;

        return $this;
    }
}