<?php

namespace GHT\ApiClientBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Defines configuration settings.
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ght_api_client');

        $rootNode
            ->children()
                ->arrayNode('curl_options')
                    ->prototype('scalar')->end()
                ->end()
                ->arrayNode('hosts')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('domain')->isRequired()->end()
                            ->integerNode('port')->defaultValue(80)->end()
                            ->booleanNode('sandbox')->defaultFalse()->end()
                            ->booleanNode('secure')->defaultFalse()->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('user_entity')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('api_name_property')->defaultValue('username')->end()
                        ->scalarNode('api_key_property')->defaultValue('apiKey')->end()
                        ->scalarNode('api_secret_property')->defaultValue('apiSecret')->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
