<?php

namespace GHT\ApiClientBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Load the GHTApiClientBundle configuration.
 */
class GHTApiClientExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();

        $config = $this->processConfiguration($configuration, $configs);

        array_walk(
            $config,
            array($this, 'setParameters'),
            array('parentKey' => $this->getAlias(), 'container' => $container)
        );

        // Load services
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config/'));
        $configuration = new Configuration();
        $loader->load('services.yml');
    }

    /**
     * {@inheritDoc}
     */
    public function getAlias()
    {
        return 'ght_api_client';
    }

    /**
     * Recursively set all the configuration values as parameters.
     *
     * @param array|string $config
     * @param string $key
     * @param array $params
     */
    public function setParameters($config, $key, $params)
    {
        // Recursively set the parameter name
        $parameterName = $params['parentKey'] . '.' . $key;

        // Set the parameter
        $params['container']->setParameter($parameterName, $config);

        // If the configuration has children, recursively call this function for each array value
        if (is_array($config)) {
            array_walk(
                $config,
                array($this, 'setParameters'),
                array('parentKey' => $parameterName, 'container' => $params['container'])
            );
        }
    }
}
