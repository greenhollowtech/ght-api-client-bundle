<?php

namespace GHT\ApiClientBundle\Tests\DependencyInjection;

use GHT\ApiClientBundle\DependencyInjection\GHTApiClientExtension;
use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionTestCase;

/**
 * Exercises the dependency injection on a compiler pass of the
 * GHTApiClientBundle.
 */
class GHTApiClientExtensionTest extends AbstractExtensionTestCase
{
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        parent::setUp();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * {@inheritdoc}
     */
    protected function getContainerExtensions()
    {
        return array(
            new GHTApiClientExtension(),
        );
    }

    /**
     * Verify that not defining a host does not throw an exception.
     */
    public function testParametersSetAfterLoadingDefaultValues()
    {
        $result = 'no exception thrown';
        try {
            $this->load();
        }
        catch (\Exception $e) {
            $result = 'exception thrown';
        }

        $this->assertEquals('no exception thrown', $result);
    }

    /**
     * Verify that parameters have been set with custom values.
     */
    public function testParametersSetAfterLoadingWithCustomValues()
    {
        $this->load(array(
            'hosts' => array(
                'prod_api' => array(
                    'domain' => 'api.greenhollowtech.com',
                    'port' => 443,
                ),
            ),
        ));

        $this->assertContainerBuilderHasParameter('ght_api_client.hosts.prod_api.port', 443);
    }

    /**
     * Verify that default values are set with custom values.
     */
    public function testParametersSetAfterLoadingDefaultAndCustomValues()
    {
        $this->load(array(
            'hosts' => array(
                'prod_api' => array(
                    'domain' => 'api.greenhollowtech.com',
                    'port' => 443,
                ),
                'sandbox_api' => array(
                    'domain' => 'sandbox.greenhollowtech.com',
                ),
            ),
        ));

        $this->assertContainerBuilderHasParameter('ght_api_client.hosts.sandbox_api.port', 80);
    }

    /**
     * Verify that attempting to define a host without a domain throws an
     * exception.
     */
    public function testRequiredDomainParameterMissing()
    {
        $this->setExpectedException(
            'Symfony\Component\Config\Definition\Exception\InvalidConfigurationException',
            'The child node "domain" at path "ght_api_client.hosts.prod_api" must be configured.'
        );

        // Trigger the exception
        $this->load(array(
            'hosts' => array(
                'prod_api' => array(
                    'port' => 443,
                ),
            ),
        ));
    }

    /**
     * Verify that default user entity parameters have been set.
     */
    public function testParametersSetAfterLoadingDefaultUserEntityValues()
    {
        $this->load(array(
            'hosts' => array(
                'prod_api' => array(
                    'domain' => 'api.greenhollowtech.com',
                ),
            ),
        ));

        $this->assertContainerBuilderHasParameter('ght_api_client.user_entity.api_key_property', 'apiKey');
    }

    /**
     * Verify that user entity parameters have been set with custom values.
     */
    public function testParametersSetAfterLoadingWithCustomUserEntityValue()
    {
        $this->load(array(
            'hosts' => array(
                'prod_api' => array(
                    'domain' => 'api.greenhollowtech.com',
                ),
            ),
            'user_entity' => array(
                'api_key_property' => 'myCustomApiKey',
            ),
        ));

        $this->assertContainerBuilderHasParameter('ght_api_client.user_entity.api_key_property', 'myCustomApiKey');
    }

    /**
     * Verify that cURL option parameters have been set with custom values.
     */
    public function testParametersSetAfterLoadingWithCustomCurlOptionValues()
    {
        $this->load(array(
            'hosts' => array(
                'prod_api' => array(
                    'domain' => 'api.greenhollowtech.com',
                ),
            ),
            'curl_options' => array(
                'connecttimeout' => 10,
                'timeout' => 20,
            ),
        ));

        $this->assertContainerBuilderHasParameter('ght_api_client.curl_options.connecttimeout', 10);
        $this->assertContainerBuilderHasParameter(
            'ght_api_client.curl_options',
            array('connecttimeout' => 10, 'timeout' => 20)
        );
    }
}