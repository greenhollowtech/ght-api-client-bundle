<?php

namespace GHT\ApiClientBundle\Tests\Service;

use GHT\ApiClientBundle\Fixtures\User;
use GHT\ApiClientBundle\Service\GHTApiClientService;

/**
 * Exercises the GHTApiClientService.
 */
class GHTApiClientServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
    }

    /**
     * Get a mocked GHTApiClient.
     *
     * @param array $methods The methods to mock.
     *
     * @return \GHT\ApiClient\GHTApiClient
     */
    protected function configureClient($methods = array())
    {
        $methods = array_merge($methods, array('delete', 'get', 'getToFile', 'post', 'postToFile', 'put'));

        $client = $this->getMockBuilder('\GHT\ApiClient\GHTApiClient')
            ->disableOriginalConstructor()
            ->setMethods($methods)
            ->getMock()
        ;

        return $client;
    }

    /**
     * Get a test host configuration.
     *
     * @return array
     */
    protected function configureHosts()
    {
        return array(
            'test_host' => array(
                'domain' => 'test.greenhollowtech.com',
                'port' => 80,
            ),
        );
    }

    /**
     * Get a test user.
     *
     * @return GHT\PhpApiBundle\Fixtures\User
     */
    protected function configureUser()
    {
        $user = new User();
        $user->setUsername('testUser')
            ->setApiKey('testKey')
            ->setApiSecret('testSecret')
        ;

        return $user;
    }

    /**
     * Verify that an exception is thrown when attempting to set the target
     * host with an unconfigured name.
     */
    public function testSetHostWithUnconfiguredName()
    {
        $apiClientService = new GHTApiClientService($this->configureHosts(), 'apiKey', 'apiSecret', 'username');
        $this->setExpectedException(
            'InvalidArgumentException', 'Host "other_host" is not configured, attempted to validate as a domain:'
        );

        // Trigger the exception
        $apiClientService->setHost('other_host');
    }

    /**
     * Verify that setting the target host with a known name does not throw an
     * exception.
     */
    public function testSetHost()
    {
        $apiClientService = new GHTApiClientService($this->configureHosts(), 'apiKey', 'apiSecret', 'username');

        $result = 'no exception thrown';
        try {
            $apiClientService->setHost('test_host');
        }
        catch (\InvalidArgumentException $e) {
            $result = 'exception thrown';
        }

        $this->assertEquals('no exception thrown', $result);
    }

    /**
     * Verify that setting the target host with a new valid domain does not
     * throw an exception.
     */
    public function testSetHostWithDomain()
    {
        $apiClientService = new GHTApiClientService($this->configureHosts(), 'apiKey', 'apiSecret', 'username');

        $result = 'no exception thrown';
        try {
            $apiClientService->setHost('timeapi.org');
        }
        catch (\InvalidArgumentException $e) {
            $result = 'exception thrown';
        }

        $this->assertEquals('no exception thrown', $result);
    }

    /**
     * Verify that attempting to set the user with an invalid property
     * configured throws an exception.
     */
    public function testSetUserWithBadPropertyName()
    {
        $apiClientService = new GHTApiClientService($this->configureHosts(), 'doesNotExist', 'apiSecret', 'username');

        $this->setExpectedException(
            'Symfony\Component\DependencyInjection\Exception\InvalidArgumentException',
            'Configured property "doesNotExist" not set in GHT\ApiClientBundle\Fixtures\User'
        );

        // Trigger the exception
        $apiClientService->setUser(new User());
    }

    /**
     * Verify that a user can be set as expected.
     */
    public function testSetUser()
    {
        $apiClientService = new GHTApiClientService($this->configureHosts(), 'apiKey', 'apiSecret', 'username');

        $result = 'no exception thrown';
        try {
            $apiClientService->setUser(new User());
        }
        catch (\InvalidArgumentException $e) {
            $result = 'exception thrown';
        }

        $this->assertEquals('no exception thrown', $result);
    }

    /**
     * Verify that setting the cURL options defines the CurlConfig entity.
     */
    public function testSetCurlOptionsDefaults()
    {
        $apiClientService = new GHTApiClientService($this->configureHosts(), 'apiKey', 'apiSecret', 'username');

        // Verify the CurlConfig is not yet set
        $reflector = new \ReflectionClass(get_class($apiClientService));
        $property = $reflector->getProperty('curlConfig');
        $property->setAccessible(true);
        $this->assertNull($property->getValue($apiClientService));

        // Set the cURL options
        $apiClientService->setCurlOptions(array('timeout' => 30));

        // Verify the CurlConfig defaults are set
        $curlConfig = $property->getValue($apiClientService);
        $this->assertInstanceOf('\GHT\ApiClient\Entity\CurlConfig', $curlConfig);
        $this->assertEquals(array('timeout' => 30), $curlConfig->getDefaultOptions());
        $this->assertEquals(array(), $curlConfig->getOverrideOptions());
    }

    /**
     * Verify that subsequent setting of the cURL options defines the
     * CurlConfig entity's override options.
     */
    public function testSetCurlOptionsOverrides()
    {
        $apiClientService = new GHTApiClientService($this->configureHosts(), 'apiKey', 'apiSecret', 'username');

        $reflector = new \ReflectionClass(get_class($apiClientService));
        $property = $reflector->getProperty('curlConfig');
        $property->setAccessible(true);

        // Set the cURL options
        $apiClientService->setCurlOptions(array('timeout' => 30));
        $apiClientService->setCurlOptions(array('timeout' => 10));

        // Verify the CurlConfig defaults are set
        $curlConfig = $property->getValue($apiClientService);
        $this->assertInstanceOf('\GHT\ApiClient\Entity\CurlConfig', $curlConfig);
        $this->assertEquals(array('timeout' => 30), $curlConfig->getDefaultOptions());
        $this->assertEquals(array('timeout' => 10), $curlConfig->getOverrideOptions());
    }

    /**
     * Verify that setting of the cURL options can be reset.
     */
    public function testSetCurlOptionsOverridesReset()
    {
        $apiClientService = new GHTApiClientService($this->configureHosts(), 'apiKey', 'apiSecret', 'username');

        $reflector = new \ReflectionClass(get_class($apiClientService));
        $property = $reflector->getProperty('curlConfig');
        $property->setAccessible(true);

        // Set the cURL options
        $apiClientService->setCurlOptions(array('timeout' => 30));
        $apiClientService->setCurlOptions(array('timeout' => 10));
        $apiClientService->setCurlOptions();

        // Verify the CurlConfig defaults are set
        $curlConfig = $property->getValue($apiClientService);
        $this->assertInstanceOf('\GHT\ApiClient\Entity\CurlConfig', $curlConfig);
        $this->assertEquals(array('timeout' => 30), $curlConfig->getDefaultOptions());
        $this->assertEquals(array(), $curlConfig->getOverrideOptions());
    }

    /**
     * Verify that attempting to get a client without setting a host throws an
     * exception.
     */
    public function testGetClientWithoutHostSet()
    {
        $apiClientService = new GHTApiClientService($this->configureHosts(), 'apiKey', 'apiSecret', 'username');

        $this->setExpectedException(
            'UnexpectedValueException',
            'API Client requires a host.'
        );

        // Trigger the exception
        $apiClientService->getClient();
    }

    /**
     * Verify that attempting to get a client without setting a user throws an
     * exception.
     */
    public function testGetClientWithoutUserSet()
    {
        $apiClientService = new GHTApiClientService($this->configureHosts(), 'apiKey', 'apiSecret', 'username');

        $this->setExpectedException(
            'UnexpectedValueException',
            'API Client requires a user with credentials.'
        );

        // Trigger the exception
        $apiClientService->setHost('test_host')->getClient();
    }

    /**
     * Verify that attempting to get a client with a user missing credentials
     * throws an exception.
     */
    public function testGetClientWithUserMissingCredentials()
    {
        $apiClientService = new GHTApiClientService($this->configureHosts(), 'apiKey', 'apiSecret', 'username');

        $user = $this->configureUser()
            ->setApiSecret()
        ;

        $this->setExpectedException(
            'UnexpectedValueException',
            'API Client requires a user with credentials.'
        );

        // Trigger the exception
        $apiClientService->setHost('test_host')->setUser($user)->getClient();
    }

    /**
     * Verify that a client can be configured as expected.
     */
    public function testGetClient()
    {
        $apiClientService = new GHTApiClientService($this->configureHosts(), 'apiKey', 'apiSecret', 'username');

        $apiClient = $apiClientService->setHost('test_host')->setUser($this->configureUser())->getClient();
    }

    /**
     * Verify that a client can be configured with cURL options as expected.
     */
    public function testGetClientWithCurlOptions()
    {
        $apiClientService = new GHTApiClientService($this->configureHosts(), 'apiKey', 'apiSecret', 'username');

        $apiClient = $apiClientService
            ->setHost('test_host')
            ->setUser($this->configureUser())
            ->setCurlOptions(array('timeout' => 30))
            ->getClient()
        ;

        $reflector = new \ReflectionClass(get_class($apiClient));
        $property = $reflector->getProperty('curlConfig');
        $property->setAccessible(true);

        // Verify the CurlConfig defaults are set
        $curlConfig = $property->getValue($apiClient);
        $this->assertInstanceOf('\GHT\ApiClient\Entity\CurlConfig', $curlConfig);
        $this->assertEquals(array('timeout' => 30), $curlConfig->getDefaultOptions());
    }

    /**
     * Verify that a custom client can be set.
     */
    public function testSetClient()
    {
        $apiClientService = new GHTApiClientService($this->configureHosts(), 'apiKey', 'apiSecret', 'username');

        $customClient = $this->configureClient(array('testCustomMethod'));

        $apiClient = $apiClientService->setHost('test_host')
            ->setUser($this->configureUser())
            ->setClient($customClient)
            ->getClient()
        ;

        $this->assertTrue(method_exists($apiClient, 'testCustomMethod'));
    }

    /**
     * Verify that the client delete method is called as expected.
     */
    public function testDelete()
    {
        $apiClientService = new GHTApiClientService($this->configureHosts(), 'apiKey', 'apiSecret', 'username');

        $client = $this->configureClient();
        $client->expects($this->once())
            ->method('delete')
            ->with(
                $this->equalTo('api/test'),
                $this->equalTo(array('test' => 'test_data'))
            )
        ;

        // Trigger the expectation
        $result = $apiClientService->setHost('test_host')
            ->setUser($this->configureUser())
            ->setClient($client)
            ->delete('api/test', array('test' => 'test_data'))
        ;
    }

    /**
     * Verify that the client get method is called as expected.
     */
    public function testGet()
    {
        $apiClientService = new GHTApiClientService($this->configureHosts(), 'apiKey', 'apiSecret', 'username');

        $client = $this->configureClient();
        $client->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo('api/test'),
                $this->equalTo(array())
            )
        ;

        // Trigger the expectation
        $result = $apiClientService->setHost('test_host')
            ->setUser($this->configureUser())
            ->setClient($client)
            ->get('api/test')
        ;
    }

    /**
     * Verify that the client getToFile method is called as expected.
     */
    public function testGetToFile()
    {
        $apiClientService = new GHTApiClientService($this->configureHosts(), 'apiKey', 'apiSecret', 'username');

        $client = $this->configureClient();
        $client->expects($this->once())
            ->method('getToFile')
            ->with(
                $this->equalTo('api/test'),
                $this->equalTo(array())
            )
        ;

        // Trigger the expectation
        $result = $apiClientService->setHost('test_host')
            ->setUser($this->configureUser())
            ->setClient($client)
            ->getToFile('api/test')
        ;
    }

    /**
     * Verify that the client post method is called as expected.
     */
    public function testPost()
    {
        $apiClientService = new GHTApiClientService($this->configureHosts(), 'apiKey', 'apiSecret', 'username');

        $client = $this->configureClient();
        $client->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo('api/test'),
                $this->equalTo(array('test' => 'test_data'))
            )
        ;

        // Trigger the expectation
        $result = $apiClientService->setHost('test_host')
            ->setUser($this->configureUser())
            ->setClient($client)
            ->post('api/test', array('test' => 'test_data'))
        ;
    }

    /**
     * Verify that the client postToFile method is called as expected.
     */
    public function testPostToFile()
    {
        $apiClientService = new GHTApiClientService($this->configureHosts(), 'apiKey', 'apiSecret', 'username');

        $client = $this->configureClient();
        $client->expects($this->once())
            ->method('postToFile')
            ->with(
                $this->equalTo('api/test'),
                $this->equalTo(array('test' => 'test_data'))
            )
        ;

        // Trigger the expectation
        $result = $apiClientService->setHost('test_host')
            ->setUser($this->configureUser())
            ->setClient($client)
            ->postToFile('api/test', array('test' => 'test_data'))
        ;
    }

    /**
     * Verify that the client put method is called as expected.
     */
    public function testPut()
    {
        $apiClientService = new GHTApiClientService($this->configureHosts(), 'apiKey', 'apiSecret', 'username');

        $client = $this->configureClient();
        $client->expects($this->once())
            ->method('put')
            ->with(
                $this->equalTo('api/test'),
                $this->equalTo(array('test' => 'test_data'))
            )
        ;

        // Trigger the expectation
        $result = $apiClientService->setHost('test_host')
            ->setUser($this->configureUser())
            ->setClient($client)
            ->put('api/test', array('test' => 'test_data'))
        ;
    }
}
